Yii2 Plivo
==========
This extension for integrate Plivo on Yii2 framework.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
Add config repository:
```
composer config repositories.yii2-plivo vcs https://bitbucket.org/rapita/yii2-plivo.git
```

Either run

```
php composer.phar require rapita/yii2-plivo:dev-master
```
or
```
composer require rapita/yii2-plivo:dev-master
```
or add

```
"rapita/yii2-plivo": "dev-master"
```

to the require section of your `composer.json` file.


Usage
-----
