/**
 * Yii Plivo Web Call. This is basic usage PlivoWebSDK. For advanced usage
 * see https://www.plivo.com/docs/sdk/web/ and customize your code.
 *
 * @see Plivo Web SDK Documentation https://www.plivo.com/docs/sdk/web/
 * @author Nazar Rapita <n.i.rapita@gmail.com>
 */
yii.plivoWebCall = (function ($) {
    var plivoWebSdk = undefined;
    var pub = {
        isActive: true,
        isCalling: false,
        inCalling: false,
        sipUsername: undefined,
        sipPassword: undefined,
        afterCallCallback: false,
        autoLogin: false,
        init: function() {
            var webkit = !!navigator.userAgent.match(/AppleWebKit\/(\d+\.\d+)/);
            if ('v'=='\v') { // Note: IE listens on document
                document.attachEvent('onstorage', inCallingCallback, false);
            } else if (window.opera || webkit){ // Note: Opera and WebKits listens on window
                window.addEventListener('storage', inCallingCallback, false);
            } else { // Note: FF listens on document.body or document
                document.body.addEventListener('storage', inCallingCallback, false);
            }

            $(window).on('beforeunload', function() {
                if (pub.isCalling === true) {
                    return true;
                }
            });

            $('body').on('click', '*[data-toggle="web-call"]', pub.callTo);
            blockWebCall();
        },
        initPlivoWebSdk: function (options) {
            if (this.hasPlivoWebSdk() == false) {
                options = $.extend({
                    debug: 'ERROR',
                    permOnClick: false,
                    enableIPV6: false,
                    codecs: ['PCMU', 'OPUS'],
                    audioConstraints: {
                        optional: [{googAutoGainControl: false}]
                    }
                }, options);

                plivoWebSdk = new window.Plivo(options);
            }

            plivoWebSdk.client.on('onWebrtcNotSupported', onWebrtcNotSupported);
            plivoWebSdk.client.on('onLogin', onLogin);
            plivoWebSdk.client.on('onLogout', onLogout);
            plivoWebSdk.client.on('onLoginFailed', onLoginFailed);
            plivoWebSdk.client.on('onCalling', onCalling);
            plivoWebSdk.client.on('onCallAnswered', onCallAnswered);
            plivoWebSdk.client.on('onCallFailed', onCallFailed);
            plivoWebSdk.client.on('onCallRemoteRinging', onCallRemoteRinging);
            plivoWebSdk.client.on('onCallTerminated', onCallTerminated);
            plivoWebSdk.client.on('onIncomingCall', onIncomingCall);
            plivoWebSdk.client.on('onIncomingCallCanceled', onIncomingCallCanceled);
            plivoWebSdk.client.on('onMediaPermission', onMediaPermission);

            if (pub.autoLogin == true) {
                pub.login();
            }
        },
        hasPlivoWebSdk: function () {
            return (plivoWebSdk != undefined);
        },
        login: function() {
            plivoWebSdk.client.login(pub.sipUsername, pub.sipPassword);
        },
        logout: function() {
            plivoWebSdk.client.logout();
        },
        call: function() {
            var $makeCall = $('#plivo-make-call');

            if ($makeCall.hasClass('disabled')) {
                return;
            }

            if ($makeCall.text() == "Call") {
                var dest = $("#plivo-call-to").val();
                if (isNotEmpty(dest)) {
                    setStatus('Calling..');
                    blockWebCall();
                    addCallParam('to', dest);
                    plivoWebSdk.client.call(dest);
                    $makeCall.text('End');
                } else {
                    setStatus('Invalid Destination');
                }
            } else if($makeCall.text() == "End") {
                setStatus('Ending..');
                plivoWebSdk.client.hangup();
                $makeCall.text('Call');
                setStatus('Ready');
            }
        },
        callTo: function () {
            var callTo = $(this).data('target');

            if ($(this).hasClass('disabled')) {
                return;
            }

            if (callTo == undefined) {
                throw new Error('Attribute `data-target` has required.')
            }

            if ($(this).hasClass('is-active') == false) {
                $('#plivo-call-to').val(callTo);
            }

            $(this).toggleClass('is-active');

            $('#plivo-make-call').trigger('click');
        },
        answer: function () {
            setStatus('Answering....');
            plivoWebSdk.client.answer();
            callAnsweredUI()
        },
        reject: function () {
            callUI();
            plivoWebSdk.client.reject();
        },
        hangup: function () {
            setStatus('Hanging up..');
            plivoWebSdk.client.hangup();
            callUI();
        },
        formatCallerId: function (callerId) {
            var indexOf = callerId.indexOf('@');
            if (indexOf > 0) {
                callerId = callerId.substring(0, indexOf);
            }
            return callerId;
        }
    };


    // ... private functions and properties go here ...
    var callUUID = undefined, callParams = [];
    
    function inCallingCallback() {
        pub.inCalling = JSON.parse(localStorage.getItem('plivoInCalling'));

        if (pub.inCalling === false) {
            unBlockWebCall();
            $('#plivo-make-call').removeClass('disabled');
        } else if (pub.inCalling === true) {
            blockWebCall();
            $('#plivo-make-call').addClass('disabled');
        }
    }
    
    function setStatus(status) {
        $('#plivo-status').text(status);
    }
    function initUI() {
        //callbox
        $('#plivo .plivo-call').hide('slow');
        $('#plivo .plivo-incoming').hide('slow');
        $('#plivo .plivo-answered').hide('slow');

        setStatus('Waiting login');

        $('#plivo .plivo-login').show('slow');
        $('#plivo .plivo-logout').hide('slow');
    }
    function callUI() {
        setStatus('Ready');
        $('#plivo-make-call').text('Call');

        $('#plivo .plivo-call').show('slow');
        $('#plivo .plivo-incoming').hide('slow');
        $('#plivo .plivo-answered').hide('slow');
    }
    function incomingCallUI(callFrom) {
        //show incoming call UI
        setStatus('Incoming Call');
        $('#plivo-call-from').text(callFrom);

        $('#plivo .plivo-call').hide('slow');
        $('#plivo .plivo-incoming').show('slow');
    }
    function callAnsweredUI() {
        $('#plivo .plivo-incoming').hide('slow');
        $('#plivo .plivo-call').hide('slow');

        $('#plivo .plivo-answered').show('slow');
    }

    function onLogin() {
        setStatus('Logged in');

        $('#plivo .plivo-login').hide('slow');
        $('#plivo .plivo-logout').show('slow');

        $('#plivo .plivo-call').show('slow');

        unBlockWebCall();
        inCallingCallback();
    }
    function onLoginFailed() {
        setStatus("Login Failed");
    }
    function onLogout() {
        initUI();
    }
    function onCalling() {
        setCalling(true);
        setStatus('Connecting....');
    }
    function onCallRemoteRinging() {
        setStatus('Ringing..');

        callUUID = plivoWebSdk.client.getCallUUID();
    }
    function onCallTerminated() {
        callUI();

        unBlockWebCall();
        handleAfterCallCallback();
    }
    function onCallFailed(cause) {
        callUI();
        setStatus("Call Failed:"+cause);
        addCallParam('status', cause);

        unBlockWebCall();
        handleAfterCallCallback();
    }
    function onMediaPermission (result) {
        if (result) {
            console.log("get media permission");
        } else {
            alert("you don't allow media permission, you will can't make a call until you allow it");
        }
    }
    function onWebrtcNotSupported() {
        setStatus("");
        alert("Your browser doesn't support WebRTC. You need Chrome 25 to use this demo");
    }
    function onCallAnswered() {
        setStatus("Call In Progress");
    }
    function onIncomingCall(account_name, extraHeaders) {
        var callFrom = pub.formatCallerId(account_name);

        incomingCallUI(callFrom);
        blockWebCall();
        callUUID = plivoWebSdk.client.getCallUUID();
        addCallParam('from', callFrom);

        setCalling(true);
    }
    function onIncomingCallCanceled() {
        callUI();

        unBlockWebCall();
        handleAfterCallCallback();
    }
    function isNotEmpty(n) {
        return n.length > 0;
    }
    function formatUSNumber(n) {
        var dest = n.replace(/-/g, '');
        dest = dest.replace(/ /g, '');
        dest = dest.replace(/\+/g, '');
        dest = dest.replace(/\(/g, '');
        dest = dest.replace(/\)/g, '');
        if (!isNaN(dest)) {
            n = dest
            if (n.length == 10 && n.substr(0, 1) != "1") {
                n = "1" + n;
            }
        }
        return n;
    }
    function replaceAll(txt, replace, with_this) {
        return txt.replace(new RegExp(replace, 'g'),with_this);
    }
    function handleAfterCallCallback() {
        setCalling(false);
        if (typeof pub.afterCallCallback === 'function' || typeof pub.afterCallCallback === 'object') {
            var call = {
                uuid: getCallUUID(),
                params: getCallParams()
            };

            pub.afterCallCallback.apply(call);
        }
    }
    function getCallUUID() {
        var uuid = callUUID;
        callUUID = undefined;
        return uuid;
    }
    function getCallParams() {
        var params = callParams;
        callParams = [];
        return params;
    }
    function addCallParam(key, value) {
        callParams[key] = value;
    }
    function blockWebCall() {
        $( "#plivo-call-to" ).prop( "disabled", true);
        $('*[data-toggle="web-call"]').not('.is-active').addClass('disabled');
    }
    function unBlockWebCall() {
        $( "#plivo-call-to" ).prop( "disabled", false);
        $('*[data-toggle="web-call"]').removeClass('disabled');
    }
    function setCalling(status) {
        if (typeof status !== 'boolean') {
            throw new Error('Status must be boolean.')
        }

        $('#plivo').trigger('plivoIsCalling', {
            status: status ? 'begin' : 'end',
            callUUID: callUUID,
            params: callParams
        });

        pub.isCalling = pub.inCalling = status;
        localStorage.setItem('plivoInCalling', status);
    }

    return pub;
})(jQuery);
