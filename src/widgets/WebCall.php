<?php

namespace rapita\plivo\widgets;

use rapita\plivo\CallerInterface;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\Html;

class WebCall extends Widget
{
    public $options = [];
    public $title = 'Plivo Web Call';
    public $afterCallCallback = false;
    public $autoLogin = true;

    private static $_hasInitialized = false;
    /**
     * @var CallerInterface $sipEndpoint
     */
    protected $sipEndpoint;

    /**
     * This is simple render button for call to number.
     * @param $address
     * @param array $options
     * @param bool $content
     * @return string
     */
    public static function buttonCallTo($address, array $options = [], $content = false)
    {
        $options['data-toggle'] = 'web-call';
        $options['data-target'] = $address;

        Html::addCssClass($options, 'btn btn-plivo-call-to');

        return Html::button($content?:'Call', $options);
    }

    public function init()
    {
        if ($this->sipEndpoint == null) {
            throw new InvalidConfigException('Property `sipEndpoint` must be set.');
        }

        if (self::$_hasInitialized !== false) {
            throw new InvalidConfigException('WebCall widget must be initialized only once.');
        }

        self::$_hasInitialized = true;

        $this->id = 'plivo';
        $this->options['id'] = $this->id;
    }

    public function run()
    {
        $view = $this->getView();
        WebCallAssetBundle::register($view);

        $sipUsername = $this->sipEndpoint->getSipUsername();
        $sipPassword = $this->sipEndpoint->getSipPassword();
        $view->registerJs("yii.plivoWebCall.sipUsername = '$sipUsername'");
        $view->registerJs("yii.plivoWebCall.sipPassword = '$sipPassword'");
        $view->registerJs("yii.plivoWebCall.autoLogin = '{$this->autoLogin}'");

        if ($this->afterCallCallback) {
            $view->registerJs("yii.plivoWebCall.afterCallCallback = $this->afterCallCallback");
        }

        $view->registerJs("yii.plivoWebCall.initPlivoWebSdk()");

        $content  = Html::beginTag('div', $this->options);

        $content .= Html::beginTag('div', ['class' => 'plivo-heading']);
        $content .= Html::tag('div', $this->title, ['class' => 'plivo-title']);

        $content .= Html::beginTag('div', ['class' => 'plivo-login']);
        $content .= Html::button('Login', ['onclick' => 'yii.plivoWebCall.login()']);
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'plivo-logout', 'style' => 'display: none']);
        $content .= Html::button('Logout', ['onclick' => 'yii.plivoWebCall.logout()']);
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'plivo-call', 'style' => 'display: none']);
        $content .= Html::beginTag('div', ['class' => 'input-group']);
        $content .= Html::input('text', 'call-to', '', [
            'id' => 'plivo-call-to',
            'class' => 'form-control',
            'placeholder' => 'Phone number or a SIP URI'
        ]);
        $content .= Html::beginTag('span', ['class' => 'input-group-btn']);
        $content .= Html::button('Call', [
            'id' => 'plivo-make-call',
            'class' => 'btn',
            'onclick' => 'yii.plivoWebCall.call()'
        ]);
        $content .= Html::endTag('span');
        $content .= Html::endTag('div');
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'plivo-incoming', 'style' => 'display: none']);
        $content .= Html::tag('span', '', ['id' => 'plivo-call-from']);
        $content .= Html::button('Answer', ['class' => 'btn btn-success btn-xs', 'onclick' => 'yii.plivoWebCall.answer()']);
        $content .= Html::button('Reject', ['class' => 'btn btn-danger btn-xs', 'onclick' => 'yii.plivoWebCall.reject()']);
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'plivo-answered', 'style' => 'display: none']);
        $content .= Html::button('Hangup', ['onclick' => 'yii.plivoWebCall.hangup()']);
        $content .= Html::endTag('div');

        $content .= Html::beginTag('div', ['class' => 'plivo-info']);
        $content .= Html::tag('span', 'Status: ', ['class' => 'status-label']);
        $content .= Html::tag('span', 'Waiting login', ['id' => 'plivo-status', 'class' => 'status-value']);
        $content .= Html::endTag('div');

        $content .= Html::endTag('div');

        return $content;
    }


    /**
     * @param CallerInterface $sipEndpoint
     * @return void
     */
    public function setSipEndpoint(CallerInterface $sipEndpoint)
    {
        $this->sipEndpoint = $sipEndpoint;
    }
}
