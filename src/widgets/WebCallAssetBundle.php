<?php

namespace rapita\plivo\widgets;

use yii\web\AssetBundle;

/**
 * Class PlivoAssetBundle
 */
class WebCallAssetBundle extends AssetBundle
{
    public $sourcePath = '@vendor/rapita/yii2-plivo/src/widgets/assets';
    public $css = [
        'css/plivoWebCall.css'
    ];
    public $js = [
        'https://s3.amazonaws.com/plivosdk/web/plivowebsdk-2.0-beta.min.js',
        'js/yii.plivoWebCall.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
