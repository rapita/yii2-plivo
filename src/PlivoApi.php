<?php

namespace rapita\plivo;

use Plivo\RestAPI;
use yii\base\Component;
use yii\base\InvalidConfigException;

/**
 * Class PlivoApi
 * @package rapita\plivo
 * Usage.
 * Add to config:
 * ```
 * 'components' => [
 *     ....
 *     'plivoApi' => [
 *         'class'     => 'rapita\plivo\PlivoApi',
 *         'authId'    => 'your auth id',
 *         'authToken' => 'your auth token',
 *     ],
 *     ....
 * ]
 * ```
 * Make results:
 * ```
 * Yii::$app->plivoApi
 *      ->callDetails([ // or other method
 *         your params
 *      ]);
 * ```
 */
class PlivoApi extends Component
{
    /** @var string $url */
    public $url = 'https://api.plivo.com';
    /** @var string $version */
    public $version = 'v1';
    /** @var string $authId */
    public $authId;
    /** @var string $authToken */
    public $authToken;
    /** @var RestAPI $client */
    private $client;

    /**
     * Initialize client
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->authId == null) {
            throw new InvalidConfigException('Property authId has null.');
        }

        if ($this->authToken == null) {
            throw new InvalidConfigException('Property authToken has null.');
        }

        if ($this->url == null) {
            throw new InvalidConfigException('Property url has null.');
        }

        if ($this->version == null) {
            throw new InvalidConfigException('Property version has null.');
        }

        $this->client = $this->createClient();
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/#get-all-call-details
     */
    public function callDetails($params = [])
    {
        return $this->client->get_cdrs($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/#get-call-detail-record
     */
    public function callRecordDetails($params = [])
    {
        return $this->client->get_cdr($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/record/#record-a-call
     */
    public function callRecordStart($params = [])
    {
        return $this->client->record($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/record/#stop-recording-a-call
     */
    public function callRecordStop($params = [])
    {
        return $this->client->stop_record($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/play/#play-a-music-file
     */
    public function callPlayStart($params = [])
    {
        return $this->client->play($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/play/#stop-playing-a-music-file
     */
    public function callPlayStop($params = [])
    {
        return $this->client->stop_play($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/speak/#play-text-during-a-call
     */
    public function callSpeakStart($params = [])
    {
        return $this->client->speak($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/speak/#stop-playing-text-during-a-call
     */
    public function callSpeakStop($params = [])
    {
        return $this->client->stop_speak($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/#get-details-of-a-live-call
     */
    public function liveCall($params = [])
    {
        return $this->client->get_live_call($params);
    }

    /**
     * @param array $params
     * @return array
     * @see https://www.plivo.com/docs/api/call/#get-all-live-calls
     */
    public function liveCalls($params = [])
    {
        return $this->client->get_live_calls($params);
    }

    /**
     * @return RestAPI
     */
    protected function createClient()
    {
        return new RestAPI($this->authId, $this->authToken, $this->url, $this->version);
    }
}
