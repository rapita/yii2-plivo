<?php

namespace rapita\plivo\migrations;

use yii\db\Migration;

class m161222_160344_web_call_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%plivo_call}}', [
            'id'  => $this->primaryKey(),
            'call_uuid' => $this->string()->notNull(),
            'type' => 'ENUM("Incoming", "Outgoing") NOT NULL',
            'duration' => $this->integer()->defaultValue(0),
            'date_call' => $this->dateTime()->defaultValue('0000-00-00 00:00:00'),
        ], $tableOptions);

        $this->createIndex('idx_call_uuid', '{{%plivo_call}}', 'call_uuid', true);
        $this->createIndex('idx_type', '{{%plivo_call}}', 'type');
        $this->createIndex('idx_duration', '{{%plivo_call}}', 'duration');
        $this->createIndex('idx_date_call', '{{%plivo_call}}', 'date_call');
    }

    public function down()
    {
        $this->dropTable('{{%plivo_call}}');
    }

}
