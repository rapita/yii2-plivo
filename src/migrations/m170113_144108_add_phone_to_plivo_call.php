<?php

namespace rapita\plivo\migrations;

use yii\db\Migration;

class m170113_144108_add_phone_to_plivo_call extends Migration
{
    public $_table = '{{%plivo_call}}';

    public function safeUp()
    {
        $this->addColumn($this->_table, 'from', $this->string());
        $this->addColumn($this->_table, 'to', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->_table, 'from');
        $this->dropColumn($this->_table, 'to');
    }
}
