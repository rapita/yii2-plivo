<?php

namespace rapita\plivo\controllers;

use rapita\plivo\models\application\ApplicationAction;
use rapita\plivo\models\directDial\Handler;
use yii\web\Controller;

/**
 * Base usage outgoing calls
 * @package rapita\plivo\controllers
 *
 */
class DirectDialController extends Controller
{
    public function actions()
    {
        return [
            'answer' => [
                'class' => ApplicationAction::class,
                'handlerClass' => Handler::class
            ]
        ];
    }
}
