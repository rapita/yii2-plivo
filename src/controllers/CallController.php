<?php

namespace rapita\plivo\controllers;

use rapita\plivo\models\application\ApplicationAction;
use rapita\plivo\models\call\IncomingAnswerHandler;
use rapita\plivo\models\call\IncomingHangupHandler;
use rapita\plivo\models\call\OutgoingAnswerHandler;
use rapita\plivo\models\call\OutgoingHangupHandler;
use yii\web\Controller;

class CallController extends Controller
{
    public function actions()
    {
        return [
            'incoming-answer' => [
                'class' => ApplicationAction::class,
                'handlerClass' => IncomingAnswerHandler::class
            ],
            'incoming-hangUp' => [
                'class' => ApplicationAction::class,
                'handlerClass' => IncomingHangupHandler::class
            ],
            'outgoing-answer' => [
                'class' => ApplicationAction::class,
                'handlerClass' => OutgoingAnswerHandler::class
            ],
            'outgoing-hangUp' => [
                'class' => ApplicationAction::class,
                'handlerClass' => OutgoingHangupHandler::class
            ],
        ];
    }
}
