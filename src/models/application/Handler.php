<?php

namespace rapita\plivo\models\application;

use Plivo\Dial;
use yii\base\Object;

/**
 * Class Handler
 * @package rapita\plivo\models\application
 */
class Handler extends Object implements HandlerInterface
{
    /**
     * @inheritdoc
     */
    public function handle(ApplicationInterface $app)
    {
        $this->buildResponse($app->getRequest(), $app->getResponseBuilder());
    }

    /**
     * This method for customize
     * @param RequestInterface $request
     * @param ResponseBuilderInterface $builder
     */
    protected function buildResponse(RequestInterface $request, ResponseBuilderInterface $builder)
    {

    }

    /**
     * @param Dial $dial
     * @param $to
     * @return void
     */
    protected function addDialTo(Dial $dial, $to)
    {
        if ($this->hasSip($to)) {
            $dial->addUser($to);
        } else {
            $dial->addNumber($to);
        }
    }

    /**
     * @param $to
     * @return bool
     */
    protected function hasSip($to)
    {
        if (is_string($to) && substr($to, 0,4) == "sip:") {
            return true;
        }
        return false;
    }

    /**
     * @param RequestInterface $request
     * @param array $params
     * @return bool
     */
    protected function inDisableCallParams(RequestInterface $request, array $params)
    {
        if (is_array($request->getDisableCall()) && in_array($request->getDisableCall(), $params)) {
            return true;
        }
        return false;
    }

    /**
     * @param RequestInterface $request
     * @return array
     */
    protected function getDialParams(RequestInterface $request)
    {
        $dialParams = [];
        if ($from = $request->getFrom(true)) {
            $dialParams['callerId'] = $from;
        }
        if ($callerName = $request->getCallerName()) {
            $dialParams['callerName'] = $callerName;
        }
        if ($dialMusic = $request->getDialMusic()) {
            $dialParams['dialMusic'] = $dialMusic;
        }
        return $dialParams;
    }
}
