<?php

namespace rapita\plivo\models\application;

use yii\base\Object;

/**
 * Class Request
 * @package rapita\plivo\models\application
 */
class Request extends Object implements RequestInterface
{
    private $CallUUID;
    private $AllParams = [];
    private $ExtraParams = [];
    private $ForwardTo;
    private $To;
    private $CLID;
    private $From;
    private $CallerName;
    private $HangupCause;
    private $Duration;
    private $DialMusic;
    private $DisableCall;
    private $CallStatus;
    private $Event;

    /**
     * @inheritdoc
     */
    public function load(array $params = [])
    {
        $this->AllParams = $params;

        foreach ($params as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            } elseif (strpos($key, 'X-PH-') !== false) {
                $this->ExtraParams[$key] = $value;
            }

            unset($params[$key]);
        }
    }

    /**
     * @inheritdoc
     */
    public function getForwardTo()
    {
        return $this->ForwardTo;
    }

    /**
     * @inheritdoc
     */
    public function getTo($withCheck = false)
    {
        if ($withCheck === true && $this->ForwardTo) {
            return $this->ForwardTo;
        }
        return $this->To;
    }

    /**
     * @inheritdoc
     */
    public function getCLID()
    {
        return $this->CLID;
    }

    /**
     * @inheritdoc
     */
    public function getFrom($withCheck = false)
    {
        if ($withCheck === true && $this->CLID) {
            return $this->CLID;
        }
        return $this->From;
    }

    /**
     * @inheritdoc
     */
    public function getCallerName()
    {
        return $this->CallerName;
    }

    /**
     * @inheritdoc
     */
    public function getHangupCause()
    {
        return $this->HangupCause;
    }

    /**
     * @inheritdoc
     */
    public function getDialMusic()
    {
        return $this->DialMusic;
    }

    /**
     * @inheritdoc
     */
    public function getDisableCall()
    {
        return $this->DisableCall;
    }

    /**
     * @inheritdoc
     */
    public function getDuration()
    {
        return $this->Duration;
    }

    /**
     * @inheritdoc
     */
    public function getCallUUID()
    {
        return $this->CallUUID;
    }

    /**
     * @inheritdoc
     */
    public function getCallStatus()
    {
        return $this->CallStatus;
    }

    /**
     * @inheritdoc
     */
    public function getEvent()
    {
        return $this->Event;
    }

    /**
     * @inheritdoc
     */
    public function getExtraParams()
    {
        return $this->ExtraParams;
    }

    /**
     * @inheritdoc
     */
    public function getAllParams()
    {
        return $this->AllParams;
    }
}