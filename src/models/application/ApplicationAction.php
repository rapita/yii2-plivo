<?php

namespace rapita\plivo\models\application;

use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * Class ApplicationAction
 * @package rapita\plivo\models\application
 */
class ApplicationAction extends Action
{
    /**
     * @var string
     */
    public $applicationClass = Application::class;
    /**
     * @var string
     */
    public $handlerClass = Handler::class;
    /**
     * @var string
     */
    public $requestClass = Request::class;
    /**
     * @var string
     */
    public $responseBuilderClass = ResponseBuilder::class;

    /**
     * @return ResponseBuilderInterface
     * @throws InvalidConfigException
     */
    public function run()
    {
        /** @var ApplicationInterface $service */
        $plivoApplication = $this->createApplication();

        if ($plivoApplication instanceof ApplicationInterface) {
            return $plivoApplication->loadRequest(\Yii::$app->request->getQueryParams())->runHandler()->getResponse();
        }

        throw new InvalidConfigException('Application must be instace of `ApplicationInterface`');
    }

    /**
     * @return ApplicationInterface
     */
    protected function createApplication()
    {
        return \Yii::createObject([
            'class' => $this->applicationClass,
            'request' => \Yii::createObject($this->requestClass),
            'responseBuilder' => \Yii::createObject($this->responseBuilderClass),
            'handler' => \Yii::createObject($this->handlerClass),
        ]);
    }
}
