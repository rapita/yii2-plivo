<?php

namespace rapita\plivo\models\application;

use yii\web\Response;

/**
 * Class ResponseFormatter
 * @package rapita\plivo\models\application
 */
class ResponseFormatter extends \yii\web\XmlResponseFormatter
{
    const FORMAT = 'plivoXML';

    /**
     * Formats the specified response.
     * @param Response $response the response to be formatted.
     */
    public function format($response)
    {
        $charset = $this->encoding === null ? $response->charset : $this->encoding;
        if (stripos($this->contentType, 'charset') === false) {
            $this->contentType .= '; charset=' . $charset;
        }
        $response->getHeaders()->set('Content-Type', $this->contentType);
        if ($response->data instanceof ResponseBuilder) {
            $response->content = $response->data->toXML();
        }
    }
}
