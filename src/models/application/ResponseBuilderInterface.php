<?php

namespace rapita\plivo\models\application;

/**
 * Interface XmlResponseBuilderInterface
 * @see https://www.plivo.com/docs/xml/response/ `XML ELEMENTS`
 */
interface ResponseBuilderInterface
{
    /**
     * Add to xml element for Speak
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addSpeak($body = null, $attributes = array());

    /**
     * Add to xml element for Play
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addPlay($body = null, $attributes = array());

    /**
     * Add to xml element for Dial
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addDial($body = null, $attributes = array());

    /**
     * Add to xml element for Number
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addNumber($body = null, $attributes = array());

    /**
     * Add to xml element for User
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addUser($body = null, $attributes = array());

    /**
     * @param array $attributes
     * @return mixed
     */
    public function addGetDigits($attributes = array());

    /**
     * @param array $attributes
     * @return mixed
     */
    public function addRecord($attributes = array());

    /**
     * @param array $attributes
     * @return mixed
     */
    public function addHangup($attributes = array());

    /**
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addRedirect($body = null, $attributes = array()) ;

    /**
     * @param array $attributes
     * @return mixed
     */
    public function addWait($attributes = array());

    /**
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addConference($body = null, $attributes = array());

    /**
     * @param array $attributes
     * @return mixed
     */
    public function addPreAnswer($attributes = array());

    /**
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addMessage($body = null, $attributes = array());

    /**
     * @param null $body
     * @param array $attributes
     * @return mixed
     */
    public function addDTMF($body = null, $attributes = array());

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param $xml
     * @return mixed
     */
    public function setAttributes($xml);

    /**
     * @param $xml
     * @return mixed
     */
    public function asChild($xml);

    /**
     * @param bool $header
     * @return string
     */
    public function toXML($header = false);
}