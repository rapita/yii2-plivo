<?php

namespace rapita\plivo\models\application;

/**
 * Interface RequestInterface
 * @see https://www.plivo.com/docs/xml/request/
 */
interface RequestInterface
{
    /**
     * This method load to object property params from request.
     * @param array $params
     * @return void
     */
    public function load(array $params = []);

    /**
     * @see getTo().
     * @return string
     */
    public function getForwardTo();

    /**
     * The phone number of the called party with the country code.
     * If the call is inbound, then it’s your incoming phone number.
     * If the call is outbound, then it’s the phone number you provided to call.
     * @param bool $withCheck
     * @return string
     */
    public function getTo($withCheck = false);

    /**
     * The caller ID from Carriers.
     * @return string
     */
    public function getCLID();

    /**
     * The phone number of the party that initiated the call along with the country code.
     * If the call is inbound, then it is the caller's caller ID. If the call is
     * outbound i.e. initiated via a request to the API, then this is
     * the phone number you specify as the caller ID.
     * @param bool $withCheck
     * @return string
     */
    public function getFrom($withCheck = false);

    /**
     * The caller name from Carriers.
     * @return string
     */
    public function getCallerName();

    /**
     * Contains the standard telephony hangup cause of a call (inbound and outbound).
     * In case the call is not hung up, this parameter will not be present.
     * @return string
     */
    public function getHangupCause();

    /**
     * @return string
     */
    public function getDialMusic();

    /**
     * @return string
     */
    public function getDisableCall();

    /**
     * @return integer
     */
    public function getDuration();

    /**
     * The unique call uuid generate with Plivo.
     * @return string
     */
    public function getCallUUID();

    /**
     * @return string
     */
    public function getCallStatus();

    /**
     * @return string
     */
    public function getEvent();

    /**
     * The extra call params added to call.
     * @return array
     */
    public function getExtraParams();

    /**
     * The all send params
     * @return array
     */
    public function getAllParams();
}