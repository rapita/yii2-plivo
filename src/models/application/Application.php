<?php

namespace rapita\plivo\models\application;

use yii\base\Object;

/**
 * Class Application
 * @package rapita\plivo\models\application
 */
class Application extends Object implements ApplicationInterface
{
    /**
     * @var RequestInterface $request
     */
    private $request;
    /**
     * @var ResponseBuilderInterface $responseBuilder
     */
    private $responseBuilder;
    /**
     * @var HandlerInterface $handler
     */
    private $handler;

    /**
     * @inheritdoc
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @inheritdoc
     */
    public function getResponse()
    {
        \Yii::$app->response->format = ResponseFormatter::FORMAT;
        return $this->getResponseBuilder();
    }

    /**
     * @inheritdoc
     */
    public function getResponseBuilder()
    {
        return $this->responseBuilder;
    }

    /**
     * @inheritdoc
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @inheritdoc
     */
    public function setRequest(RequestInterface $request)
    {
        $this->request = $request;
    }

    /**
     * @inheritdoc
     */
    public function setResponseBuilder(ResponseBuilderInterface $responseBuilder)
    {
        $this->responseBuilder = $responseBuilder;
    }

    /**
     * @inheritdoc
     */
    public function setHandler(HandlerInterface $handler)
    {
        $this->handler = $handler;
    }

    /**
     * @inheritdoc
     */
    public function loadRequest(array $params = [])
    {
        \Yii::info($params, 'plivo');
        $this->request->load($params);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function runHandler()
    {
        $this->handler->handle($this);
        return $this;
    }

}
