<?php

namespace rapita\plivo\models\application;

/**
 * Interface HandlerInterface
 * @package rapita\plivo\models\application
 */
interface HandlerInterface
{
    /**
     * This method for handle logic
     * @param ApplicationInterface $app
     * @return mixed
     */
    public function handle(ApplicationInterface $app);
}
