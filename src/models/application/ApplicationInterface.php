<?php

namespace rapita\plivo\models\application;

/**
 * Interface DirectDialServiceInterface
 */
interface ApplicationInterface
{
    /**
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * @return ResponseBuilderInterface and set to yii request formatter
     */
    public function getResponse();

    /**
     * @return ResponseBuilderInterface
     */
    public function getResponseBuilder();

    /**
     * @return HandlerInterface
     */
    public function getHandler();

    /**
     * @param RequestInterface $request
     * @return void
     */
    public function setRequest(RequestInterface $request);

    /**
     * @param ResponseBuilderInterface $responseBuilder
     * @return void
     */
    public function setResponseBuilder(ResponseBuilderInterface $responseBuilder);

    /**
     * @param HandlerInterface $handler
     * @return void
     */
    public function setHandler(HandlerInterface $handler);

    /**
     * @param array $params
     * @return $this
     */
    public function loadRequest(array $params = []);

    /**
     * @return $this
     */
    public function runHandler();
}
