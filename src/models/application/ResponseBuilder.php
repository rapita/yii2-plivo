<?php

namespace rapita\plivo\models\application;

use Plivo\Element;

/**
 * Class PlivoResponseBuilder extends default Plivo Response class.
 */
class ResponseBuilder extends Element implements ResponseBuilderInterface
{
    protected $nestables = array(
        'Speak',
        'Play',
        'GetDigits',
        'Record',
        'Dial',
        'Redirect',
        'Wait',
        'Hangup',
        'PreAnswer',
        'Conference',
        'DTMF',
        'Message'
    );

    function __construct() {
        parent::__construct(null);
        $this->name = 'Response';
    }

    public function toXML($header = false) {
        return parent::toXML(true);
    }
}
