<?php

namespace rapita\plivo\models\call;

use yii\db\Connection;
use yii\db\Query;

/**
 * Class CallRepository
 * @package rapita\plivo\models\call
 */
class CallRepository
{
    /**
     * @var Connection
     */
    private $db;

    /**
     * CallRepository constructor.
     * @param Connection $db
     */
    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    /**
     * @param PlivoCall $call
     * @return int
     */
    public function save(PlivoCall $call)
    {
        if ($this->exist($call)) {
            return $this->update($call);
        } else {
            return $this->insert($call);
        }
    }

    /**
     * @param PlivoCall $call
     * @return int
     */
    public function insert(PlivoCall $call)
    {
        return $this->db->createCommand()->insert('plivo_call', [
            'from' => $call->getFrom(),
            'to' => $call->getTo(),
            'call_uuid' => $call->getUuid(),
            'type' => $call->getType(),
            'date_call' => $call->getDateCall(),
        ])->execute();
    }

    /**
     * @param PlivoCall $call
     * @return int
     */
    public function update(PlivoCall $call)
    {
        return $this->db->createCommand()->update('plivo_call', [
            'duration' => $call->getDuration()
        ], [
            'call_uuid' => $call->getUuid(),
        ])->execute();
    }

    /**
     * @param PlivoCall $call
     * @return bool
     */
    public function exist(PlivoCall $call)
    {
        return (new Query())
            ->from('plivo_call')
            ->where(['call_uuid' => $call->getUuid()])
            ->exists($this->db);
    }
}
