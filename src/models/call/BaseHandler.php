<?php

namespace rapita\plivo\models\call;

use Plivo\Dial;
use rapita\plivo\models\application\ApplicationInterface;
use rapita\plivo\models\application\Handler;
use rapita\plivo\models\application\RequestInterface;
use rapita\plivo\models\application\ResponseBuilderInterface;

/**
 * Class BaseHandler
 * @package rapita\plivo\models\call
 */
abstract class BaseHandler extends Handler
{
    /**
     * @var CallRepository
     */
    protected $callRepository;

    /**
     * @param RequestInterface $request
     * @return PlivoCall
     */
    abstract protected function createCall(RequestInterface $request);

    /**
     * For create Repository
     */
    public function init()
    {
        $this->callRepository = new CallRepository(\Yii::$app->getDb());
    }

    /**
     * @param ApplicationInterface $app
     * @return void
     */
    public function handle(ApplicationInterface $app)
    {
        parent::handle($app);

        $call = $this->createCall($app->getRequest());
        $this->callRepository->save($call);
    }

    /**
     * This is base response for doing call
     * @param RequestInterface $request
     * @param ResponseBuilderInterface $builder
     * @return void
     */
    protected function buildResponse(RequestInterface $request, ResponseBuilderInterface $builder)
    {
        $to = $this->getTo($request);
        if ($to) {
            $dialParams = $this->getDialParams($request);
            /** @var Dial $dial */
            $dial = $builder->addDial($dialParams);
            $this->addDialTo($dial, $to);
        } else {
            $builder->addHangup();
        }
    }

    /**
     * @param RequestInterface $request
     * @return string
     */
    protected function getTo(RequestInterface $request)
    {
        return $request->getTo(true);
    }

    /**
     * @param $sipUsername
     * @return string
     */
    public function formatSip($sipUsername)
    {
        return strstr(str_replace('sip:', '', $sipUsername), '@', true);
    }
}
