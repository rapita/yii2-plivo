<?php

namespace rapita\plivo\models\call;

use rapita\plivo\models\application\RequestInterface;

class IncomingHangupHandler extends BaseHandler
{
    /**
     * @inheritdoc
     */
    protected function createCall(RequestInterface $request)
    {
        $params = $request->getAllParams();
        return new PlivoCall($request->getFrom(), $request->getTo(), $request->getCallUUID(), PlivoCall::TYPE_INCOMING, [
            'duration' => empty($params['duration']) ? 0 : $params['duration']
        ]);
    }
}
