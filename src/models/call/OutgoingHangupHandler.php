<?php

namespace rapita\plivo\models\call;

use rapita\plivo\models\application\RequestInterface;

class OutgoingHangupHandler extends BaseHandler
{
    /**
     * @inheritdoc
     */
    protected function createCall(RequestInterface $request)
    {
        $params = $request->getAllParams();
        return new PlivoCall($request->getFrom(), $request->getTo(), $request->getCallUUID(), PlivoCall::TYPE_OUTGOING, [
            'duration' => empty($params['duration']) ? 0 : $params['duration']
        ]);
    }
}
