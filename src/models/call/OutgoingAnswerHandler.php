<?php

namespace rapita\plivo\models\call;

use rapita\plivo\models\application\RequestInterface;

class OutgoingAnswerHandler extends BaseHandler
{
    /**
     * @inheritdoc
     */
    protected function createCall(RequestInterface $request)
    {
        return new PlivoCall($request->getFrom(), $request->getTo(), $request->getCallUUID(), PlivoCall::TYPE_OUTGOING, [
            'dateCall' => date('Y-m-d H:i:s')
        ]);
    }
}
