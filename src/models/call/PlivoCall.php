<?php

namespace rapita\plivo\models\call;

use yii\base\Arrayable;
use yii\base\Exception;
use yii\base\Object;

/**
 * Class PlivoCall
 * @package rapita\plivo\models\call
 */
class PlivoCall extends Object
{
    const TYPE_INCOMING = 'Incoming';
    const TYPE_OUTGOING = 'Outgoing';

    /**
     * @var string $uuid
     */
    protected $uuid;
    /**
     * @var string $type
     */
    protected $type;
    /**
     * @var integer $duration
     */
    protected $duration;
    /**
     * @var string $dateCall
     */
    protected $dateCall;
    /**
     * @var string $from
     */
    protected $from;
    /**
     * @var string $to
     */
    protected $to;

    /**
     * PlivoCall constructor.
     * @param string $form
     * @param string $to
     * @param string $uuid
     * @param string $type
     * @param array $config
     * @throws Exception
     */
    public function __construct($form, $to, $uuid, $type, $config = [])
    {
        parent::__construct($config);

        if (!$this->checkUuid($uuid)) {
            throw new Exception("Bad a uuid: $uuid.");
        }

        if (!$this->checkType($type)) {
            throw new Exception("Bad a type: $type.");
        }

        $this->uuid = $uuid;
        $this->type = $type;
        $this->from = $form;
        $this->to = $to;
    }

    /**
     * @param $type
     * @return bool
     */
    public function checkType($type)
    {
        return $type === self::TYPE_INCOMING || $type === self::TYPE_OUTGOING;
    }

    /**
     * @param $uuid
     * @return bool
     */
    public function checkUuid($uuid)
    {
        return strlen($uuid) == 36;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @return string
     */
    public function getDateCall()
    {
        return $this->dateCall;
    }

    /**
     * @param $value
     * @return void
     */
    public function setDuration($value)
    {
        $this->duration = $value;
    }

    /**
     * @param $value
     * @return void
     */
    public function setDateCall($value)
    {
        $this->dateCall = $value;
    }
}
