<?php

namespace rapita\plivo\models\call;

use rapita\plivo\models\application\RequestInterface;

class IncomingAnswerHandler extends BaseHandler
{
    /**
     * @inheritdoc
     */
    protected function createCall(RequestInterface $request)
    {
        return new PlivoCall($request->getFrom(), $request->getTo(), $request->getCallUUID(), PlivoCall::TYPE_INCOMING, [
            'dateCall' => date('Y-m-d H:i:s')
        ]);
    }
}
