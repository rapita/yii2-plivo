<?php

namespace rapita\plivo;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'rapita\plivo\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
//        if (Yii::$app instanceof \yii\console\Application) {
//            $this->controllerNamespace = 'rapita\plivo\commands';
//        }
    }
}
