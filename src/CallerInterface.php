<?php

namespace rapita\plivo;

/**
 * Interface CallerInterface
 */
interface CallerInterface
{
    /**
     * This is Sip Endpoint `username`
     * @return string
     */
    public function getSipUsername();

    /**
     * This is Sip Endpoint `password`
     * @return string
     */
    public function getSipPassword();

    /**
     * This is phone number
     * @return string
     */
    public function getPhoneFrom();
}
